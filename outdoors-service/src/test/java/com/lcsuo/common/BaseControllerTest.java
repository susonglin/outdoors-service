package com.lcsuo.common;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.alibaba.fastjson.JSONObject;
import com.lcsuo.utils.RestfulResponseBody;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration(value = "src/main/webapp")
@ContextConfiguration(locations = { "classpath:/spring-mybatis.xml", "classpath:/spring-mvc.xml" })
public class BaseControllerTest {

	public String token;
	
	@Autowired
	WebApplicationContext context;
	
	public MockMvc mvc;
	
	@Before
	public void initTests() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
		login();
	}

	public void login() throws Exception {
		String mobileId = "13141489332";
		String password = "123456";

		String content = mvc.perform(post("/api/user/login").servletPath("/api/user/login").param("mobileId", mobileId).param("password", password))
				.andDo(print()).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		RestfulResponseBody body = (RestfulResponseBody) JSONObject.parseObject(content, RestfulResponseBody.class);
		token = body.getData().get("token").toString();
	}

}
