package com.lcsuo.service.impl;

import java.io.IOException;
import java.io.InputStream;

import javax.annotation.Resource;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.lcsuo.bean.User;
import com.lcsuo.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)     //表示继承了SpringJUnit4ClassRunner类  
@ContextConfiguration(locations = {"classpath:spring-mybatis.xml"})  
public class UserServiceImplTest {

	private static Logger logger = Logger.getLogger(UserServiceImplTest.class);  
	
	@Resource
	private UserService userService = null;
	
	@Test
	public void testGet() throws IOException {
		User user = userService.get(1);
		User user1 = userService.get(1);
		User user2 = userService.get(1);
		User user3 = userService.get(1);
		logger.info("值："+user.getUserName()+"===>"+user1.getUserName());
	}

}
