package com.lcsuo.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.springframework.http.MediaType;

import com.alibaba.fastjson.JSONObject;
import com.lcsuo.bean.ActivityLeader;
import com.lcsuo.bean.ThemeAggregate;
import com.lcsuo.bean.vo.ActivityVo;
import com.lcsuo.common.BaseControllerTest;

public class ThemeActivityControllerTest extends BaseControllerTest{

	@Test
	public void testSaveActivity() throws Exception {
		
		ActivityVo vo = new ActivityVo();
		vo.setThemeId(1);//关联主题表的主键
		vo.setBegintime(new Date());
		vo.setEndtime(new Date());
		vo.setActivitycost(new BigDecimal(120));
		vo.setAdvancecost(new BigDecimal(50));
		vo.setPlancount(50);
		vo.setTraffictype(1);//大巴车
		List<ActivityLeader> list = new ArrayList<>(2);
		
		//设置领队信息
		ActivityLeader bean = new ActivityLeader();
		bean.setAccountLeader(1);//第一个领队
		bean.setIsaddsalary(1);//补贴薪资
		bean.setAddsalary(new BigDecimal(400));
		list.add(bean);
		ActivityLeader bean1 = new ActivityLeader();
		bean1.setAccountLeader(2);//第二个领队
		bean1.setIsaddsalary(1);//补贴薪资
		bean1.setAddsalary(new BigDecimal(200));
		list.add(bean1);
		
		//设置集合地信息
		List<ThemeAggregate> listAgg = new ArrayList<>(2);
		ThemeAggregate aggBean1 = new ThemeAggregate();
		aggBean1.setCollectionplace("惠新西街南口");
		aggBean1.setCollectiontime(new Date());
		listAgg.add(aggBean1);
		ThemeAggregate aggBean2 = new ThemeAggregate();
		aggBean2.setCollectionplace("惠新西街南口");
		aggBean2.setCollectiontime(new Date());
		listAgg.add(aggBean2);
		
		vo.setList(list);
		vo.setListAggregate(listAgg);
		
		String jsonArray = JSONObject.toJSONString(vo);
		mvc.perform(post("/api/activity/saveActivity?t="+token).
				 contentType(MediaType.APPLICATION_JSON)
				.content(jsonArray)).andExpect(status().isOk())
				.andExpect(jsonPath("$.resultFlag").value(1))
				.andDo(print());
		
	}
	
	@Test
	public void testFindActivity() throws Exception {
		mvc.perform(get("/api/activity/findActivity").param("t", token)
				).andExpect(status().isOk()).andDo(print()); 
		
	}

}
