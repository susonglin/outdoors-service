package com.lcsuo.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.Test;
import org.springframework.http.MediaType;

import com.alibaba.fastjson.JSONObject;
import com.lcsuo.bean.AccountLeader;
import com.lcsuo.common.BaseControllerTest;
import com.lcsuo.utils.Constants;

public class AccountLeaderControllerTest extends BaseControllerTest{

	@Test
	public void testSaveAccountLeader() throws Exception {
		AccountLeader accountLeader = new AccountLeader();
		accountLeader.setLevel(Constants.leaderLevel.NUMONE);
		accountLeader.setSalary(new BigDecimal(200));
		
		String accountId = "1";
		
		String jsonArray = JSONObject.toJSONString(accountLeader);
		
		mvc.perform(post("/api/accountLeader/saveLeader?t="+token).param("accountId",accountId).
				 contentType(MediaType.APPLICATION_JSON)
				.content(jsonArray)).andExpect(status().isOk())
				.andExpect(jsonPath("$.resultFlag").value(1))
				.andDo(print());
	}

}
