package com.lcsuo.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.MediaType;

import com.alibaba.fastjson.JSONObject;
import com.lcsuo.bean.GroupTeam;
import com.lcsuo.common.BaseControllerTest;

public class GroupTeamControllerTest extends BaseControllerTest{
	
	@Test
	public void testSaveTeam() throws Exception {
		GroupTeam bean = new GroupTeam();
		bean.setGroupname("野蘑菇");
		bean.setFullname("野蘑菇户外旅游社");
		bean.setLinkman("向钰");
		bean.setContact("13456787654");
		bean.setIntroduce("我们专注于户外");
		
		String jsonArray = JSONObject.toJSONString(bean);
		
		mvc.perform(post("/api/team/saveTeam?t="+token).
				 contentType(MediaType.APPLICATION_JSON)
				.content(jsonArray)).andExpect(status().isOk())
				.andExpect(jsonPath("$.resultFlag").value(1))
				.andDo(print());
	}

}
