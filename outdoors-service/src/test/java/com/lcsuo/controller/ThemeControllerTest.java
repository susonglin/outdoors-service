package com.lcsuo.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.MediaType;

import com.alibaba.fastjson.JSONObject;
import com.lcsuo.bean.Theme;
import com.lcsuo.common.BaseControllerTest;

public class ThemeControllerTest extends BaseControllerTest{

	@Test
	public void testSaveTheme() throws Exception {
		Theme bean = new Theme();
		bean.setName("十渡漂流");
		bean.setPlace("北京十渡辖区");
		bean.setIntroduce("十渡是北京著名景点，漂流爽得一比");
		bean.setJourney("7点集合，九点到十渡");
		bean.setEquipadvise("带上换洗衣物");
		bean.setAgerange("10岁到60岁之间均可");
		
		String jsonArray = JSONObject.toJSONString(bean);
		
		mvc.perform(post("/api/theme/saveTheme?t="+token).
				 contentType(MediaType.APPLICATION_JSON)
				.content(jsonArray)).andExpect(status().isOk())
				.andExpect(jsonPath("$.resultFlag").value(1))
				.andDo(print());
		
	}

}
