package com.lcsuo.controller;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.springframework.http.MediaType;

import com.alibaba.fastjson.JSONObject;
import com.lcsuo.bean.Account;
import com.lcsuo.bean.ThemeAggregate;
import com.lcsuo.bean.ThemeUser;
import com.lcsuo.bean.vo.ThemeUserVo;
import com.lcsuo.common.BaseControllerTest;

public class ThemeUserControllerTest extends BaseControllerTest{

	@Test
	public void testInsertThemeUser() throws Exception {
		ThemeUserVo vo = new ThemeUserVo();
		String themeActivityId = "1";
		//添加用户信息
		List<Account> accountList = new ArrayList<>();
		Account accountBean = new Account();
		accountBean.setName("杨四海");
		accountBean.setMobileid("134111111111");
		accountList.add(accountBean);
		vo.setAccount(accountList);
		
		//添加报名信息
		List<ThemeUser> themeUserList = new ArrayList<>();
		ThemeUser themeUserBean = new ThemeUser();
		themeUserBean.setPersonnumber(1);//一人
		themeUserBean.setIspay(1);//已预付费
		themeUserList.add(themeUserBean);
		vo.setThemeUser(themeUserList);
		
		//添加集合地信息
		List<ThemeAggregate> themeAggregateList = new ArrayList<>();
		ThemeAggregate themeAggregateBean = new ThemeAggregate();
		themeAggregateBean.setCollectionplace("温都水城");
		themeAggregateBean.setCollectiontime(new Date());
		themeAggregateList.add(themeAggregateBean);
		vo.setThemeAggregate(themeAggregateList);
		
		String jsonArray = JSONObject.toJSONString(vo);
		
		mvc.perform(post("/api/theme/insertThemeUser?t="+token)
				.param("themeActivityId", themeActivityId)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonArray)).andExpect(status().isOk())
				.andExpect(jsonPath("$.resultFlag").value(1))
				.andDo(print());
	}

}
