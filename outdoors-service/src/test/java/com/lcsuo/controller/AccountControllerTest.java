package com.lcsuo.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;

import com.lcsuo.common.BaseControllerTest;

public class AccountControllerTest extends BaseControllerTest{

	public AccountControllerTest() throws Exception {
		super();
	}

	@Test
	public void testRegister() throws Exception {
		String mobileId = "131414893311";
		String password = "123456";
		String role = "1";
		mvc.perform(post("/api/user/register")
				.param("mobileId",mobileId)
				.param("password", password)
				.param("role", role)
				).andExpect(status().isOk()).andDo(print()); 
	}

	@Test
	public void testLogin() throws Exception {
		String mobileId = "13141489332";
		String password = "123456";
		mvc.perform(post("/api/user/login")
				.param("mobileId",mobileId)
				.param("password", password)
				).andExpect(status().isOk()).andDo(print()); 
		
	}

}
