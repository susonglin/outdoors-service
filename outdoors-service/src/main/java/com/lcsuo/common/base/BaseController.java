package com.lcsuo.common.base;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.lcsuo.bean.Account;
import com.lcsuo.service.AccountService;
import com.lcsuo.utils.RestfulResponseBody;

/**
 * controller公共类
 * @author Sorin
 *
 */
public class BaseController {
	
	@Autowired
	private AccountService accountService;

	public RestfulResponseBody setResponseEntity(String reason,Integer resultFlag) {
		RestfulResponseBody body = new RestfulResponseBody();
		body.setReason(reason);
		body.setResultFlag(resultFlag);
		return body;
	}
	
	/**
	 * 获取当前登录用户
	 * @return
	 */
	public Account getUser(){
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		Integer userid = (Integer) request.getAttribute("userId");
		return accountService.get(userid);
	}
}
