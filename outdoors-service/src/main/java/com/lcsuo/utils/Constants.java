package com.lcsuo.utils;
/**
 * 常量类
 * @author Sorin
 *
 */
public class Constants {
	
	public static class resultType{
		/**
		 * 成功的返回
		 */
		public static final Integer SUCCESS = 1;
		/**
		 * 失败的返回
		 */
		public static final Integer ERROR = 0;
	}
	
	public static class publicStatus{
		/**
		 * 有效
		 */
		public static final Integer EFFECTIVE = 1;
		
		/**
		 * 无效
		 */
		public static final Integer INVALID = 0;
	}
	
	public static class leaderLevel{
		/**
		 * 正领队
		 */
		public static final Integer NUMONE = 1;
		/**
		 * 副领队
		 */
		public static final Integer NUMTWO = 2;
	}
	
	public static class roleType{
		
		public static final String user = "1";
	}
}
