package com.lcsuo.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.util.StringUtils;

/**
 * @author Tom
 */
public class DateUtils {

	private final static String ZERO = "0";
	
	/** 日期格式: yyyy-MM-dd */
	public static final String date_format1 = "yyyy-MM-dd";
	/** 日期格式: yyyy-MM-dd HH:mm:ss */
	public static final String date_format2 = "yyyy-MM-dd HH:mm:ss";
	
	public final static SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
	public final static SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd HH:mm:ss");

	public String getNowString() {
		StringBuffer buffer = new StringBuffer();
		Calendar calendar = getCalendar();
		buffer.delete(0, buffer.capacity());
		buffer.append(getYear(calendar));

		if (getMonth(calendar) < 10) {
			buffer.append(ZERO);
		}
		buffer.append(getMonth(calendar));

		if (getDate(calendar) < 10) {
			buffer.append(ZERO);
		}
		buffer.append(getDate(calendar));
		if (getHour(calendar) < 10) {
			buffer.append(ZERO);
		}
		buffer.append(getHour(calendar));
		if (getMinute(calendar) < 10) {
			buffer.append(ZERO);
		}
		buffer.append(getMinute(calendar));
		if (getSecond(calendar) < 10) {
			buffer.append(ZERO);
		}
		buffer.append(getSecond(calendar));
		return buffer.toString();
	}

	private static int getDateField(Date date, int field) {
		Calendar c = getCalendar();
		c.setTime(date);
		return c.get(field);
	}

	public static int getYearsBetweenDate(Date begin, Date end) {
		int bYear = getDateField(begin, Calendar.YEAR);
		int eYear = getDateField(end, Calendar.YEAR);
		return eYear - bYear;
	}

	public static int getMonthsBetweenDate(Date begin, Date end) {
		int bMonth = getDateField(begin, Calendar.MONTH);
		int eMonth = getDateField(end, Calendar.MONTH);
		return eMonth - bMonth;
	}

	public static int getWeeksBetweenDate(Date begin, Date end) {
		int bWeek = getDateField(begin, Calendar.WEEK_OF_YEAR);
		int eWeek = getDateField(end, Calendar.WEEK_OF_YEAR);
		return eWeek - bWeek;
	}

	/**
	 * 当前时间距今月份数
	 * 
	 * @param begin
	 * @param end
	 * @return
	 */
	public static int getMonthsBetween(Date begin, Date end) {
		int year = getYearsBetweenDate(begin, end);
		int month = getMonthsBetweenDate(begin, end);
		return (year * 12 + month);
	}

	/**
	 * 时间相差天数
	 * @param begin 开始时间
	 * @param end 结束时间
	 * @return
	 */
	public static int getDaysBetweenDate(Date begin, Date end) {
		return (int) ((end.getTime() - begin.getTime()) / (1000 * 60 * 60 * 24));
	}

	public static void main(String args[]) {
		String cudate = "2013-05-23";
		System.out.println(getThedayBeforeAt1700(cudate));
	}

	/**
	 * 获取date年后的amount年的第一天的开始时间
	 * 
	 * @param amount
	 *            可正、可负
	 * @return
	 */
	public static Date getSpecficYearStart(Date date, int amount) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.YEAR, amount);
		cal.set(Calendar.DAY_OF_YEAR, 1);
		return getStartDate(cal.getTime());
	}

	/**
	 * 获取date年后的amount年的最后一天的终止时间
	 * 
	 * @param amount
	 *            可正、可负
	 * @return
	 */
	public static Date getSpecficYearEnd(Date date, int amount) {
		Date temp = getStartDate(getSpecficYearStart(date, amount + 1));
		Calendar cal = Calendar.getInstance();
		cal.setTime(temp);
		cal.add(Calendar.DAY_OF_YEAR, -1);
		return getFinallyDate(cal.getTime());
	}

	/**
	 * 获取date月后的amount月的第一天的开始时间
	 * 
	 * @param amount
	 *            可正、可负
	 * @return
	 */
	public static Date getSpecficMonthStart(Date date, int amount) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, amount);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return getStartDate(cal.getTime());
	}

	/**
	 * 获取当前自然月后的amount月的最后一天的终止时间
	 * 
	 * @param amount
	 *            可正、可负
	 * @return
	 */
	public static Date getSpecficMonthEnd(Date date, int amount) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(getSpecficMonthStart(date, amount + 1));
		cal.add(Calendar.DAY_OF_YEAR, -1);
		return getFinallyDate(cal.getTime());
	}

	/**
	 * 获取date周后的第amount周的开始时间（这里星期一为一周的开始）
	 * 
	 * @param amount
	 *            可正、可负
	 * @return
	 */
	public static Date getSpecficWeekStart(Date date, int amount) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.setFirstDayOfWeek(Calendar.MONDAY); /* 设置一周的第一天为星期一 */
		cal.add(Calendar.WEEK_OF_MONTH, amount);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		return getStartDate(cal.getTime());
	}

	/**
	 * 获取date周后的第amount周的最后时间（这里星期日为一周的最后一天）
	 * 
	 * @param amount
	 *            可正、可负
	 * @return
	 */
	public static Date getSpecficWeekEnd(Date date, int amount) {
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek(Calendar.MONDAY); /* 设置一周的第一天为星期一 */
		cal.add(Calendar.WEEK_OF_MONTH, amount);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		return getFinallyDate(cal.getTime());
	}

	public static Date getSpecficDateStart(Date date, int amount) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_YEAR, amount);
		return getStartDate(cal.getTime());
	}

	/**
	 * 得到指定日期的一天的的最后时刻23:59:59
	 * 
	 * @param date
	 * @return
	 */
	public static Date getFinallyDate(Date date) {
		String temp = format.format(date);
		temp += " 23:59:59";

		try {
			return format1.parse(temp);
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * 得到指定日期的一天的开始时刻00:00:00
	 * 
	 * @param date
	 * @return
	 */
	public static Date getStartDate(Date date) {
		String temp = format.format(date);
		temp += " 00:00:00";

		try {
			return format1.parse(temp);
		} catch (Exception e) {
			return null;
		}
	}
	/**
	 * 得到前一天的17：00
	 * 如今天是2016-3-16
	 * 结果是2016-3-15 17：00
	 * @param date
	 * @return
	 */
	public static String getThedayBeforeAt1700(String date) {
		
		Date dateFormat = getFormatDate(date);
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateFormat);
		cal.add(Calendar.DAY_OF_YEAR,-1);
		SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
		String temp = sd.format(cal.getTime());
		temp += " 17:00:00";

		try {
			return temp;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * @param date
	 *            指定比较日期
	 * @param compareDate
	 * @return
	 */
	public static boolean isInDate(Date date, Date compareDate) {
		if (compareDate.after(getStartDate(date)) && compareDate.before(getFinallyDate(date))) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * 获取两个时间的差值秒
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static Integer getSecondBetweenDate(Date d1, Date d2) {
		Long second = (d2.getTime() - d1.getTime()) / 1000;
		return second.intValue();
	}

	public static Integer getComNow(Date d1, Date d2) {
		Long second = (d2.getTime() - d1.getTime()) / 1000;

		return second.intValue();
	}

	private int getYear(Calendar calendar) {
		return calendar.get(Calendar.YEAR);
	}

	private int getMonth(Calendar calendar) {
		return calendar.get(Calendar.MONDAY) + 1;
	}

	private int getDate(Calendar calendar) {
		return calendar.get(Calendar.DATE);
	}

	private int getHour(Calendar calendar) {
		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	private int getMinute(Calendar calendar) {
		return calendar.get(Calendar.MINUTE);
	}

	private int getSecond(Calendar calendar) {
		return calendar.get(Calendar.SECOND);
	}

	private static Calendar getCalendar() {
		return Calendar.getInstance();
	}

	/**
	 * 将指定日期对象转换成格式化字符串
	 * @param date 日期对象
	 * @param datePattern 日期格式
	 * @return String
	 */
	public static String getFormattedString(Date date, String datePattern) {
		SimpleDateFormat sd = new SimpleDateFormat(datePattern);
		return sd.format(date);
	}

	/**
	 * 默认时间格式为 yyyy-MM-dd HH:mm:ss
	 * @param date
	 * @return
	 */
	public static String getFormattedString(Date date) {
		SimpleDateFormat sd = new SimpleDateFormat(date_format2);
		return sd.format(date);
	}
	
	/**
	 * 返回时间格式为 yyyy-MM-dd
	 * @return
	 */
	public static String getYMDString(Date date) {
		SimpleDateFormat sd = new SimpleDateFormat(date_format1);
		return sd.format(date);
	}
	
	/**
	 * 将原格式的字符串时间转换为指定格式的字符串时间
	 * @param date 日期
	 * @param originalPattern 原日期格式
	 * @param datePattern 新日期格式
	 * @return
	 */
	public static String converDateString(String date, String originalPattern, String datePattern) {
		if (StringUtils.isEmpty(date)) return "";
		Date originalDate = getFormatDate(date, originalPattern);
		return getFormattedString(originalDate, datePattern);
	}

	/**
	 * 将指定字符串转换成日期
	 * @param date 日期字符串
	 * @param datePattern 日期格式
	 * @return
	 */
	public static java.util.Date getFormatDate(String date, String datePattern) {
		SimpleDateFormat sd = new SimpleDateFormat(datePattern);
		return sd.parse(date, new java.text.ParsePosition(0));
	}

	/**
	 * 获取Interval个月后的时间
	 * 
	 * @param startDate
	 * @param Interval
	 * @return
	 */
	public static Date getthreeDate(Date startDate, int Interval) {
		Calendar c = Calendar.getInstance();
		c.setTime(startDate);
		c.add(Calendar.MONTH, Interval); // 将当前日期加,以月为单位
		Date validityDate = c.getTime();
		return validityDate;
	}

	public static java.util.Date getFormatDate(String date) {
		SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
		return sd.parse(date, new java.text.ParsePosition(0));
	}

}
