package com.lcsuo.utils;

import java.util.HashMap;
import java.util.Map;

public class RestfulResponseBody {

	private int resultFlag;
	private String reason;
	private Map<String, Object> data;

	public RestfulResponseBody() {
		this.reason = "";
		this.data = new HashMap<String, Object>();
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	public void put(String key, Object value) {
		data.put(key, value);
	}

	public int getResultFlag() {
		return resultFlag;
	}

	public void setResultFlag(int resultFlag) {
		this.resultFlag = resultFlag;
	}

	

}
