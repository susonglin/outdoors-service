package com.lcsuo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lcsuo.bean.Account;
import com.lcsuo.bean.AccountLeader;
import com.lcsuo.bean.GroupTeam;
import com.lcsuo.bean.ThemeActivity;
import com.lcsuo.bean.vo.ActivityVo;
import com.lcsuo.common.base.BaseController;
import com.lcsuo.service.AccountLeaderService;
import com.lcsuo.service.GroupTeamService;
import com.lcsuo.service.ThemeActivityService;
import com.lcsuo.utils.Constants;
import com.lcsuo.utils.RestfulResponseBody;

@RestController
@RequestMapping("/api/activity")
public class ThemeActivityController extends BaseController{

	@Autowired
	private ThemeActivityService service;
	
	@Autowired
	private GroupTeamService groupTeamService;
	
	@Autowired
	private ThemeActivityService themeActivityService;
	
	
	/**
	 * 新建活动
	 * @param themeActivity
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveActivity")
	public RestfulResponseBody saveActivity(@RequestBody ActivityVo activityVo){
		//获取当前登录用户
		Account user = getUser();
		//查询当前登录用户已创建团队
		GroupTeam groupTeam = groupTeamService.findByAccountId(user.getId());
		if(groupTeam == null){
			return setResponseEntity("还没有创建团队！", Constants.resultType.ERROR);
		}
		activityVo.setGroupId(groupTeam.getId());
		service.saveActivity(activityVo);
		return setResponseEntity("保存成功！", Constants.resultType.SUCCESS);
	}
	
	/**
	 * 查询活动列表
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/findActivity")
	public RestfulResponseBody findActivity(){
		//获取当前登录用户
		Account user = getUser();
		List<ThemeActivity> themeActivityList = themeActivityService.findByAccountId(user.getId());
		RestfulResponseBody body = setResponseEntity("查询成功！", Constants.resultType.SUCCESS);
		body.put("themeActivityList", themeActivityList);
		return body;
	}
}
