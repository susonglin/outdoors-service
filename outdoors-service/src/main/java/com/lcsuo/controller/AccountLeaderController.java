package com.lcsuo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lcsuo.bean.Account;
import com.lcsuo.bean.AccountLeader;
import com.lcsuo.bean.GroupTeam;
import com.lcsuo.common.base.BaseController;
import com.lcsuo.service.AccountLeaderService;
import com.lcsuo.service.GroupTeamService;
import com.lcsuo.utils.Constants;
import com.lcsuo.utils.RestfulResponseBody;

@RestController
@RequestMapping("/api/accountLeader")
public class AccountLeaderController extends BaseController{

	@Autowired
	private AccountLeaderService service;
	
	@Autowired
	private GroupTeamService groupTeamService;
	
	/**
	 * 保存领队信息
	 * @param accountLeader
	 * @param accountId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveLeader")
	public RestfulResponseBody saveAccountLeader(@RequestBody AccountLeader accountLeader, String accountId){
		//获取当前用户
		Account user = getUser();
		//查询当前登录用户已创建团队
		GroupTeam groupTeam = groupTeamService.findByAccountId(user.getId());
		if(groupTeam == null){
			return setResponseEntity("还没有创建团队！", Constants.resultType.ERROR);
		}
		//保存领队信息
		service.saveAccountLeader(accountLeader, groupTeam, accountId);
		return setResponseEntity("保存成功！", Constants.resultType.SUCCESS);
	}
	
}
