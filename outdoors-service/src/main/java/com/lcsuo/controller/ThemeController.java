package com.lcsuo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lcsuo.bean.Account;
import com.lcsuo.bean.GroupTeam;
import com.lcsuo.bean.Theme;
import com.lcsuo.common.base.BaseController;
import com.lcsuo.service.GroupTeamService;
import com.lcsuo.service.ThemeService;
import com.lcsuo.utils.Constants;
import com.lcsuo.utils.RestfulResponseBody;

@RestController
@RequestMapping("/api/theme")
public class ThemeController extends BaseController{
	
	@Autowired
	private ThemeService themeService;
	
	@Autowired
	private GroupTeamService groupTeamService;
	
	/**
	 * 创建团队
	 * @param theme
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveTheme")
	public RestfulResponseBody saveTheme(@RequestBody Theme theme){
		//获取当前用户
		Account user = getUser();
		//查询当前登录用户已创建团队
		GroupTeam groupTeam = groupTeamService.findByAccountId(user.getId());
		if(groupTeam == null){
			return setResponseEntity("还没有创建团队！", Constants.resultType.ERROR);
		}
		themeService.saveTheme(theme, groupTeam.getId(), user);
		return setResponseEntity("保存成功！", Constants.resultType.SUCCESS);
	}
	
}
