package com.lcsuo.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lcsuo.bean.User;
import com.lcsuo.service.UserService;
import com.lcsuo.utils.RestfulResponseBody;

@Controller
@RequestMapping("/user")  
public class UserController {
	@Resource
	private UserService userService;
	
	@RequestMapping("/showUser")
	public String toIndex(HttpServletRequest request,Model model,String id){
		int userId = Integer.parseInt(id);  
        User user = this.userService.get(userId);  
        model.addAttribute("user", user);  
        return "showUser";
	}
	
	@ResponseBody
	@RequestMapping("/showUser_test")
	public RestfulResponseBody test(HttpServletRequest request,Model model,String id){
		RestfulResponseBody body = new RestfulResponseBody();
		body.setReason("Hello");
		body.setResultFlag(1);
		return body;
	}
	
}
