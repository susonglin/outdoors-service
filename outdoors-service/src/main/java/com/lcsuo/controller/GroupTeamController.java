package com.lcsuo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lcsuo.bean.Account;
import com.lcsuo.bean.GroupTeam;
import com.lcsuo.common.base.BaseController;
import com.lcsuo.service.GroupTeamService;
import com.lcsuo.utils.Constants;
import com.lcsuo.utils.RestfulResponseBody;

@RestController
@RequestMapping("/api/team")
public class GroupTeamController extends BaseController{
	
	@Autowired
	private GroupTeamService groupTeamService;

	/**
	 * 保存团队信息
	 * @param groupTeam
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveTeam", method=RequestMethod.POST)
	public RestfulResponseBody saveTeam(@RequestBody GroupTeam groupTeam){
		//获取当前登录用户信息
		Account user = getUser();
		groupTeamService.saveTeam(groupTeam, user);
		return setResponseEntity("保存成功！", Constants.resultType.SUCCESS);
	}
}
