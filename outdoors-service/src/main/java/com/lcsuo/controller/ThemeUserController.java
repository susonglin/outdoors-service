package com.lcsuo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lcsuo.bean.vo.ThemeUserVo;
import com.lcsuo.common.base.BaseController;
import com.lcsuo.service.ThemeUserService;
import com.lcsuo.utils.Constants;
import com.lcsuo.utils.RestfulResponseBody;

@RestController
@RequestMapping("/api/theme")
public class ThemeUserController extends BaseController{
	
	@Autowired
	private ThemeUserService service;

	/**
	 * 添加活动人员
	 * @param themeUserVo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/insertThemeUser")
	public RestfulResponseBody insertThemeUser(@RequestBody(required=true)ThemeUserVo themeUserVo, String themeActivityId){
		service.insertThemeUser(themeUserVo, Integer.parseInt(themeActivityId));
		return setResponseEntity("保存成功！", Constants.resultType.SUCCESS);
	}
}
