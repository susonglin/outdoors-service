package com.lcsuo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lcsuo.common.base.BaseController;
import com.lcsuo.service.ThemeVehicleService;
import com.lcsuo.utils.Constants;
import com.lcsuo.utils.RestfulResponseBody;

@RestController
@RequestMapping("/api/vehicle")
public class ThemeVehicleController extends BaseController{
	
	@Autowired
	private ThemeVehicleService service;
	
	/**
	 * 添加活动车辆
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/saveVehicle")
	public RestfulResponseBody saveVehicle(
			@RequestParam(value="themeActivityId", required=true)Integer themeActivityId, 
			@RequestParam(value="accountDriverId", required=true)Integer accountDriverId){
		service.saveVehicle(themeActivityId, accountDriverId);
		return setResponseEntity("保存成功", Constants.resultType.SUCCESS);
	}
}
