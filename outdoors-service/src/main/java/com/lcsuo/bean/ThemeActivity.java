package com.lcsuo.bean;

import java.io.Serializable;
import java.util.Date;

public class ThemeActivity implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 6063456833545358280L;

	private Integer id;

    private Integer themeId;

    private Date begintime;

    private Date endtime;

    private Long activitycost;

    private Long advancecost;

    private Integer plancount;

    private Integer actualcount;

    private Integer traffictype;

    private Date createtime;

    private Integer groupId;

    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getThemeId() {
        return themeId;
    }

    public void setThemeId(Integer themeId) {
        this.themeId = themeId;
    }

    public Date getBegintime() {
        return begintime;
    }

    public void setBegintime(Date begintime) {
        this.begintime = begintime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public Long getActivitycost() {
        return activitycost;
    }

    public void setActivitycost(Long activitycost) {
        this.activitycost = activitycost;
    }

    public Long getAdvancecost() {
        return advancecost;
    }

    public void setAdvancecost(Long advancecost) {
        this.advancecost = advancecost;
    }

    public Integer getPlancount() {
        return plancount;
    }

    public void setPlancount(Integer plancount) {
        this.plancount = plancount;
    }

    public Integer getActualcount() {
        return actualcount;
    }

    public void setActualcount(Integer actualcount) {
        this.actualcount = actualcount;
    }

    public Integer getTraffictype() {
        return traffictype;
    }

    public void setTraffictype(Integer traffictype) {
        this.traffictype = traffictype;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}