package com.lcsuo.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Sorin
 *
 */
public class Theme implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -5486618000586355503L;

	private Integer id;

    private Integer groupId;

    private Integer createperson;

    private String name;

    private String place;

    private String journey;

    private String introduce;

    private String equipadvise;

    private String agerange;

    private String picimage;

    private Integer status;

    private Date createtime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getCreateperson() {
        return createperson;
    }

    public void setCreateperson(Integer createperson) {
        this.createperson = createperson;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place == null ? null : place.trim();
    }

    public String getJourney() {
        return journey;
    }

    public void setJourney(String journey) {
        this.journey = journey == null ? null : journey.trim();
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce == null ? null : introduce.trim();
    }

    public String getEquipadvise() {
        return equipadvise;
    }

    public void setEquipadvise(String equipadvise) {
        this.equipadvise = equipadvise == null ? null : equipadvise.trim();
    }

    public String getAgerange() {
        return agerange;
    }

    public void setAgerange(String agerange) {
        this.agerange = agerange == null ? null : agerange.trim();
    }

    public String getPicimage() {
        return picimage;
    }

    public void setPicimage(String picimage) {
        this.picimage = picimage == null ? null : picimage.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
}