package com.lcsuo.bean;

import java.io.Serializable;

public class ThemeVehicle implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -5376847590562772238L;

	private Integer themeActivityId;

    private Integer accountDriverId;

    public Integer getThemeActivityId() {
        return themeActivityId;
    }

    public void setThemeActivityId(Integer themeActivityId) {
        this.themeActivityId = themeActivityId;
    }

    public Integer getAccountDriverId() {
        return accountDriverId;
    }

    public void setAccountDriverId(Integer accountDriverId) {
        this.accountDriverId = accountDriverId;
    }
}