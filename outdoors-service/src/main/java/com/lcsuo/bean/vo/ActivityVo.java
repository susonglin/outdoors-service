package com.lcsuo.bean.vo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.lcsuo.bean.ActivityLeader;
import com.lcsuo.bean.ThemeAggregate;

/**
 * 添加活动VO
 * @author Sorin
 *
 */
/**
 * @author Administrator
 *
 */
public class ActivityVo {

	//关联主题表的主键
	private Integer themeId;

    private Date begintime;

    private Date endtime;
    //活动费用
    private BigDecimal activitycost;
    //活动预付费用
    private BigDecimal advancecost;
    //预计人数
    private Integer plancount;
    //实际人数
    private Integer actualcount;
    //交通方式
    private Integer traffictype;
    //关联团队，后台获取
    private Integer groupId;
    //领队可以有多个
    private List<ActivityLeader> list;
    //集合地可以有多个 
    private List<ThemeAggregate> listAggregate;
    
	public List<ThemeAggregate> getListAggregate() {
		return listAggregate;
	}

	public void setListAggregate(List<ThemeAggregate> listAggregate) {
		this.listAggregate = listAggregate;
	}

	public Integer getThemeId() {
		return themeId;
	}

	public void setThemeId(Integer themeId) {
		this.themeId = themeId;
	}

	public Date getBegintime() {
		return begintime;
	}

	public void setBegintime(Date begintime) {
		this.begintime = begintime;
	}

	public Date getEndtime() {
		return endtime;
	}

	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}

	public BigDecimal getActivitycost() {
		return activitycost;
	}

	public void setActivitycost(BigDecimal activitycost) {
		this.activitycost = activitycost;
	}

	public BigDecimal getAdvancecost() {
		return advancecost;
	}

	public void setAdvancecost(BigDecimal advancecost) {
		this.advancecost = advancecost;
	}

	public Integer getPlancount() {
		return plancount;
	}

	public void setPlancount(Integer plancount) {
		this.plancount = plancount;
	}

	public Integer getActualcount() {
		return actualcount;
	}

	public void setActualcount(Integer actualcount) {
		this.actualcount = actualcount;
	}

	public Integer getTraffictype() {
		return traffictype;
	}

	public void setTraffictype(Integer traffictype) {
		this.traffictype = traffictype;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public List<ActivityLeader> getList() {
		return list;
	}

	public void setList(List<ActivityLeader> list) {
		this.list = list;
	}
}
