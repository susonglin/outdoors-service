package com.lcsuo.bean.vo;

import java.util.List;

import com.lcsuo.bean.Account;
import com.lcsuo.bean.ThemeAggregate;
import com.lcsuo.bean.ThemeUser;

public class ThemeUserVo {
	
	//活动ID
	private String themeActivityId;

	//添加的用户，可能添加多个
	private List<Account> account;
	//报名信息
	private List<ThemeUser> themeUser;
	//添加活动集合地信息
	private List<ThemeAggregate> themeAggregate;
	
	public List<ThemeAggregate> getThemeAggregate() {
		return themeAggregate;
	}
	public void setThemeAggregate(List<ThemeAggregate> themeAggregate) {
		this.themeAggregate = themeAggregate;
	}
	public List<Account> getAccount() {
		return account;
	}
	public void setAccount(List<Account> account) {
		this.account = account;
	}
	public List<ThemeUser> getThemeUser() {
		return themeUser;
	}
	public void setThemeUser(List<ThemeUser> themeUser) {
		this.themeUser = themeUser;
	}
	public String getThemeActivityId() {
		return themeActivityId;
	}
	public void setThemeActivityId(String themeActivityId) {
		this.themeActivityId = themeActivityId;
	}
}
