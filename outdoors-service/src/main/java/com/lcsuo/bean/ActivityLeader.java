package com.lcsuo.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class ActivityLeader implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 2400812578890135364L;

	private Integer accountLeader;

    private Integer themeActivityId;

    private Integer isaddsalary;

    private BigDecimal addsalary;

    public Integer getAccountLeader() {
        return accountLeader;
    }

    public void setAccountLeader(Integer accountLeader) {
        this.accountLeader = accountLeader;
    }

    public Integer getThemeActivityId() {
        return themeActivityId;
    }

    public void setThemeActivityId(Integer themeActivityId) {
        this.themeActivityId = themeActivityId;
    }

    public Integer getIsaddsalary() {
        return isaddsalary;
    }

    public void setIsaddsalary(Integer isaddsalary) {
        this.isaddsalary = isaddsalary;
    }

    public BigDecimal getAddsalary() {
        return addsalary;
    }

    public void setAddsalary(BigDecimal addsalary) {
        this.addsalary = addsalary;
    }
}