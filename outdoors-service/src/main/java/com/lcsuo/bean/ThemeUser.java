package com.lcsuo.bean;

import java.io.Serializable;
import java.util.Date;

public class ThemeUser implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 7575852855763876185L;

	private Integer id;

    private Integer accountUserId;

    private Integer themeActivityId;

    private Integer personnumber;

    private Integer status;

    private Integer ispay;

    private Date createtime;

    private Integer themeAggregateId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccountUserId() {
        return accountUserId;
    }

    public void setAccountUserId(Integer accountUserId) {
        this.accountUserId = accountUserId;
    }

    public Integer getThemeActivityId() {
        return themeActivityId;
    }

    public void setThemeActivityId(Integer themeActivityId) {
        this.themeActivityId = themeActivityId;
    }

    public Integer getPersonnumber() {
        return personnumber;
    }

    public void setPersonnumber(Integer personnumber) {
        this.personnumber = personnumber;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIspay() {
        return ispay;
    }

    public void setIspay(Integer ispay) {
        this.ispay = ispay;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Integer getThemeAggregateId() {
        return themeAggregateId;
    }

    public void setThemeAggregateId(Integer themeAggregateId) {
        this.themeAggregateId = themeAggregateId;
    }
}