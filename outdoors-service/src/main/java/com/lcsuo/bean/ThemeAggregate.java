package com.lcsuo.bean;

import java.io.Serializable;
import java.util.Date;

public class ThemeAggregate implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 8368174892132726388L;

	private Integer id;

    private Integer themeActivityId;

    private String collectionplace;

    private Integer status;

    private Date collectiontime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getThemeActivityId() {
        return themeActivityId;
    }

    public void setThemeActivityId(Integer themeActivityId) {
        this.themeActivityId = themeActivityId;
    }

    public String getCollectionplace() {
        return collectionplace;
    }

    public void setCollectionplace(String collectionplace) {
        this.collectionplace = collectionplace == null ? null : collectionplace.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCollectiontime() {
        return collectiontime;
    }

    public void setCollectiontime(Date collectiontime) {
        this.collectiontime = collectiontime;
    }
}