package com.lcsuo.bean.authority;

import java.util.Date;

public class Token {
    private String token;

    private Integer userid;

    private String companyid;

    private String agentcode;

    private String ip;

    private Date lastvisittime;

    private Integer lastupdatorid;

    private Date lastupdatetime;

    private Date createtime;

    private Integer no;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token == null ? null : token.trim();
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getCompanyid() {
        return companyid;
    }

    public void setCompanyid(String companyid) {
        this.companyid = companyid == null ? null : companyid.trim();
    }

    public String getAgentcode() {
        return agentcode;
    }

    public void setAgentcode(String agentcode) {
        this.agentcode = agentcode == null ? null : agentcode.trim();
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    public Date getLastvisittime() {
        return lastvisittime;
    }

    public void setLastvisittime(Date lastvisittime) {
        this.lastvisittime = lastvisittime;
    }

    public Integer getLastupdatorid() {
        return lastupdatorid;
    }

    public void setLastupdatorid(Integer lastupdatorid) {
        this.lastupdatorid = lastupdatorid;
    }

    public Date getLastupdatetime() {
        return lastupdatetime;
    }

    public void setLastupdatetime(Date lastupdatetime) {
        this.lastupdatetime = lastupdatetime;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }
}