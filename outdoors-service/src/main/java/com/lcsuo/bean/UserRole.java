package com.lcsuo.bean;

import java.io.Serializable;

public class UserRole implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 4946920336169238120L;

	private Integer accountId;

    private Integer roleId;

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}