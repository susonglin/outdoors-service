package com.lcsuo.service;

import com.lcsuo.bean.User;

public interface UserService {
	
	public User get(Integer id);
}
