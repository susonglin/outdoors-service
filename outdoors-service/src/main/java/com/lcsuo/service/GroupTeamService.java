package com.lcsuo.service;

import com.lcsuo.bean.Account;
import com.lcsuo.bean.GroupTeam;

public interface GroupTeamService {

	public void saveTeam(GroupTeam groupTeam, Account account);
	
	public GroupTeam findByAccountId(Integer accountId);
}
