package com.lcsuo.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lcsuo.bean.AccountLeader;
import com.lcsuo.bean.ActivityLeader;
import com.lcsuo.bean.ThemeActivity;
import com.lcsuo.bean.ThemeAggregate;
import com.lcsuo.bean.vo.ActivityVo;
import com.lcsuo.dao.ActivityLeaderMapper;
import com.lcsuo.dao.ThemeActivityMapper;
import com.lcsuo.dao.ThemeAggregateMapper;
import com.lcsuo.service.AccountLeaderService;
import com.lcsuo.service.ActivityLeaderService;
import com.lcsuo.service.ThemeActivityService;
import com.lcsuo.utils.Constants;

@Service
public class ThemeActivityServiceImpl implements ThemeActivityService {
	
	@Autowired
	private ThemeActivityMapper dao;
	
	@Autowired
	private ActivityLeaderMapper activityLeaderMapper;
	
	@Autowired
	private ThemeAggregateMapper themeAggregateMapper;
	
	

	@Override
	public void saveActivity(ActivityVo activityVo) {
		//保存活动表
		int ThemeActivity_id = saveThemeActivity(activityVo);
		//保存活动_领队表
		saveActivityLeader(activityVo,ThemeActivity_id);
		//保存活动_集合地表
		saveThemeAggregate(activityVo,ThemeActivity_id);
	}
	
	/**
	 * 保存活动_集合地表
	 * @param activityVo
	 * @param ThemeActivity_id
	 */
	public void saveThemeAggregate(ActivityVo activityVo, int ThemeActivity_id){
		ThemeAggregate bean = new ThemeAggregate();
		List<ThemeAggregate> listAggregate = activityVo.getListAggregate();
		for (ThemeAggregate themeAggregate : listAggregate) {
			bean.setThemeActivityId(ThemeActivity_id);
			bean.setCollectionplace(themeAggregate.getCollectionplace());
			bean.setStatus(Constants.publicStatus.EFFECTIVE);//设置有效
			bean.setCollectiontime(themeAggregate.getCollectiontime());
			themeAggregateMapper.insert(bean);
		}
	}
	
	/**
	 * 保存活动_领队关联表
	 * @param activityVo
	 */
	private void saveActivityLeader(ActivityVo activityVo, int ThemeActivity_id){
		List<ActivityLeader> list = activityVo.getList();
		ActivityLeader bean = new ActivityLeader();
		BeanUtils.copyProperties(activityVo, bean);
		//保存多个领队信息
		for (ActivityLeader activityLeader : list) {
			bean.setAccountLeader(activityLeader.getAccountLeader());//关联领队
			bean.setThemeActivityId(ThemeActivity_id);
			bean.setIsaddsalary(activityLeader.getIsaddsalary());
			bean.setAddsalary(activityLeader.getAddsalary());//金额
			activityLeaderMapper.insert(bean);
		}
	}
	
	/**
	 * 保存活动表
	 * @param activityVo
	 * @return 返回插入的主键值
	 */
	private int saveThemeActivity(ActivityVo activityVo){
		ThemeActivity bean = new ThemeActivity();
		BeanUtils.copyProperties(activityVo, bean);
		bean.setActualcount(0);//实际报名人数初始值
		bean.setCreatetime(new Date());
		return dao.insert(bean);
	}

	@Override
	public List<ThemeActivity> findByAccountId(Integer accountId) {
		return dao.findByAccountId(accountId);
	}

}
