package com.lcsuo.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lcsuo.bean.Account;
import com.lcsuo.bean.GroupTeam;
import com.lcsuo.dao.GroupTeamMapper;
import com.lcsuo.service.GroupTeamService;
import com.lcsuo.utils.Constants;

@Service
public class GroupTeamServiceImpl implements GroupTeamService {

	@Autowired
	private GroupTeamMapper dao;
	
	@Override
	public void saveTeam(GroupTeam groupTeam, Account account) {
		groupTeam.setAccountId(account.getId());
		groupTeam.setStatus(Constants.publicStatus.EFFECTIVE);
		groupTeam.setCreatetime(new Date());
		dao.insert(groupTeam);
	}

	@Override
	public GroupTeam findByAccountId(Integer accountId) {
		return dao.findByAccountId(accountId);
	}

}
