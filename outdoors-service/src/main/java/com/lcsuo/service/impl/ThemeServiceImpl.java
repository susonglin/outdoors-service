package com.lcsuo.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lcsuo.bean.Account;
import com.lcsuo.bean.Theme;
import com.lcsuo.dao.ThemeMapper;
import com.lcsuo.service.ThemeService;
import com.lcsuo.utils.Constants;

@Service
public class ThemeServiceImpl implements ThemeService{
	
	@Autowired
	private ThemeMapper dao;

	/**
	 * 新建主题
	 */
	@Override
	public void saveTheme(Theme theme, Integer groupId, Account user) {
		theme.setGroupId(groupId);//关联团队表
		theme.setCreateperson(user.getId());
		theme.setStatus(Constants.publicStatus.EFFECTIVE);
		theme.setCreateperson(user.getId());
		theme.setCreatetime(new Date());
		dao.insert(theme);
	}
}
