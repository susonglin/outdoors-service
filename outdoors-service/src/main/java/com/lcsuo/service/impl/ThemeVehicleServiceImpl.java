package com.lcsuo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lcsuo.bean.ThemeVehicle;
import com.lcsuo.dao.ThemeVehicleMapper;
import com.lcsuo.service.ThemeVehicleService;

@Service
public class ThemeVehicleServiceImpl implements ThemeVehicleService {

	@Autowired
	private ThemeVehicleMapper dao;
	
	@Override
	public void saveVehicle(Integer themeActivityId, Integer accountDriverId) {
		ThemeVehicle bean = new ThemeVehicle();
		bean.setAccountDriverId(accountDriverId);
		bean.setThemeActivityId(themeActivityId);
		dao.insert(bean);
	}
}
