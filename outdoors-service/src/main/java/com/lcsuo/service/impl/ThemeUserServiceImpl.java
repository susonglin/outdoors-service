package com.lcsuo.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lcsuo.bean.Account;
import com.lcsuo.bean.ThemeAggregate;
import com.lcsuo.bean.ThemeUser;
import com.lcsuo.bean.vo.ThemeUserVo;
import com.lcsuo.dao.AccountMapper;
import com.lcsuo.dao.ThemeAggregateMapper;
import com.lcsuo.dao.ThemeUserMapper;
import com.lcsuo.service.AccountService;
import com.lcsuo.service.ThemeUserService;
import com.lcsuo.utils.Constants;
import com.lcsuo.utils.seculity.Md5PwdEncoder;

@Service
public class ThemeUserServiceImpl implements ThemeUserService{
	
	@Autowired
	private AccountMapper accountMapper;
	
	@Autowired
	private ThemeUserMapper themeUserMapper;
	
	@Autowired
	private ThemeAggregateMapper themeAggregateMapper;

	@Override
	public void insertThemeUser(ThemeUserVo themeUserVo, Integer themeActivityId) {
		//添加用户
		List<Integer> saveAccountList = saveAccount(themeUserVo);
		//添加活动集合地信息
		List<Integer> saveThemeAggregateList = saveThemeAggregate(themeUserVo, themeActivityId);
		//添加活动报名信息
		saveThemeUser(themeUserVo, themeActivityId, saveThemeAggregateList, saveAccountList);
	}
	
	/**
	 * 添加用户
	 * @param themeUserVo
	 */
	public List<Integer> saveAccount(ThemeUserVo themeUserVo){
		Account bean = new Account();
		List<Account> accountList = themeUserVo.getAccount();
		List<Integer> saveResult = new ArrayList<>(); 
		for (Account account : accountList) {
			BeanUtils.copyProperties(account, bean);
			bean.setPassword(new Md5PwdEncoder().encodePassword("123456"));
			bean.setStatus(Constants.publicStatus.EFFECTIVE);
			bean.setCreatetime(new Date());
			int reslut = accountMapper.insert(bean);
			saveResult.add(reslut);
		}
		return saveResult;
	}
	
	/**
	 * 添加活动报名信息
	 * @param themeUserVo
	 */
	public void saveThemeUser(ThemeUserVo themeUserVo, Integer themeActivityId, List<Integer> saveThemeAggregateList,
			List<Integer> saveAccountList){
		ThemeUser bean = new ThemeUser();
		List<ThemeUser> themeUserList = themeUserVo.getThemeUser();
		for (ThemeUser themeUser : themeUserList) {
			BeanUtils.copyProperties(themeUser, bean);
			bean.setCreatetime(new Date());
			bean.setThemeActivityId(themeActivityId);//关联活动
			for (Integer result : saveThemeAggregateList) {//关联活动_集合地
				bean.setThemeAggregateId(result);
			}
			for (Integer result : saveAccountList) {//关联活动_集合地
				bean.setAccountUserId(result);
			}
			bean.setStatus(Constants.publicStatus.EFFECTIVE);
			themeUserMapper.insert(bean);
		}
	}
	
	/**
	 * 添加活动集合地信息
	 * @param themeUserVo
	 */
	public List<Integer> saveThemeAggregate(ThemeUserVo themeUserVo, Integer themeActivityId){
		ThemeAggregate bean = new ThemeAggregate();
		List<ThemeAggregate> themeAggregateList = themeUserVo.getThemeAggregate();
		List<Integer> saveResult = new ArrayList<>(); 
		for (ThemeAggregate themeAggregate : themeAggregateList) {
			BeanUtils.copyProperties(themeAggregate, bean);
			bean.setThemeActivityId(themeActivityId);
			bean.setStatus(Constants.publicStatus.EFFECTIVE);
			int insert = themeAggregateMapper.insert(bean);
			saveResult.add(insert);
		}
		return saveResult;
	}

}
