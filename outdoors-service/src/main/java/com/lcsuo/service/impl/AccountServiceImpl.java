package com.lcsuo.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lcsuo.bean.Account;
import com.lcsuo.dao.AccountMapper;
import com.lcsuo.service.AccountService;
import com.lcsuo.utils.seculity.Md5PwdEncoder;

@Service
public class AccountServiceImpl implements AccountService{

	@Autowired
	private AccountMapper dao;
	
	
	@Override
	public Account get(Integer id) {
		return dao.selectByPrimaryKey(id);
	}
	
	public int insert(Account account){
		return dao.insert(account);
	}
	
	public void update(Account account){
		dao.updateByPrimaryKeySelective(account);
	}
	
	public void delete(Integer id){
		dao.deleteByPrimaryKey(id);
	}

	@Override
	public void register(String mobileId, String password, String role) {
		//密码做MD5加密处理
		password = new Md5PwdEncoder().encodePassword(password);
		Account account = new Account();
		account.setMobileid(mobileId);
		account.setPassword(password);
		account.setCreatetime(new Date());
		insert(account);
	}

	@Override
	public Account login(String mobileId, String password) {
		return dao.findByMobileId(mobileId);
	}
	
	

}
