package com.lcsuo.service.impl;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lcsuo.bean.User;
import com.lcsuo.dao.RedisDao;
import com.lcsuo.dao.UserMapper;
import com.lcsuo.service.UserService;

/**
 * @author Sorin
 *
 */
@Service 
public class UserServiceImpl implements UserService{

	@Resource
	private UserMapper dao;
	
	@Autowired
	private RedisDao redisDao;
	
	@Override
	public User get(Integer id) {
		//定时器，定时更新redis
		//redis缓存优化
		User user = redisDao.getRedis(id);
		if(user == null){//缓存为空
			user = dao.selectByPrimaryKey(id);//查询数据库
			if(user != null){
				redisDao.putRedis(user);//更新缓存
			}
		}
		return user;
	}
}
