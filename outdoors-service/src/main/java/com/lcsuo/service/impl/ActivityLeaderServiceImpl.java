package com.lcsuo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lcsuo.bean.AccountLeader;
import com.lcsuo.bean.ActivityLeader;
import com.lcsuo.bean.ThemeActivity;
import com.lcsuo.dao.ActivityLeaderMapper;
import com.lcsuo.service.ActivityLeaderService;

@Service
public class ActivityLeaderServiceImpl implements ActivityLeaderService {

	@Autowired
	private ActivityLeaderMapper dao;
	@Override
	public void save(ActivityLeader activityLeader, AccountLeader accountLeader, ThemeActivity themeActivity) {
		activityLeader.setAccountLeader(accountLeader.getId());//关联领队表
		activityLeader.setThemeActivityId(themeActivity.getId());//关联活动表
		dao.insert(activityLeader);
	}
	@Override
	public List<ActivityLeader> findByAccountLeader(Integer accountLeader) {
		return dao.findByAccountLeader(accountLeader);
	}
	@Override
	public List<ActivityLeader> findByThemeActivityId(Integer themeActivityId) {
		return dao.findByThemeActivityId(themeActivityId);
	}

}
