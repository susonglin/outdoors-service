package com.lcsuo.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lcsuo.bean.AccountLeader;
import com.lcsuo.bean.ActivityLeader;
import com.lcsuo.bean.GroupTeam;
import com.lcsuo.dao.AccountLeaderMapper;
import com.lcsuo.service.AccountLeaderService;
import com.lcsuo.service.ActivityLeaderService;
import com.lcsuo.utils.Constants;

@Service
public class AccountLeaderServiceImpl implements AccountLeaderService {

	@Autowired
	private AccountLeaderMapper dao;
	
	@Autowired
	private ActivityLeaderService activityLeaderService;
	
	/**
	 * 新增领队
	 */
	@Override
	public void saveAccountLeader(AccountLeader accountLeader, GroupTeam groupTeam, String accountId) {
		accountLeader.setGroupId(groupTeam.getId());//关联团队ID
		accountLeader.setAccountId(Integer.parseInt(accountId));//关联领队ID
		accountLeader.setCreatetime(new Date());
		accountLeader.setStatus(Constants.publicStatus.EFFECTIVE);
		dao.insert(accountLeader);
	}

}
