package com.lcsuo.service.authority;

import com.lcsuo.bean.authority.Token;

public interface TokenService {

	/**
	 * 
	 * 验证Token是否有效, 判断条件: token, 浏览器版本信息, ip
	 * @param t 用户携带的token值
	 * @param agentCode 浏览器版本信息
	 * @param ip ip地址
	 * @return 有效的token对象
	 */
	public Token checkToken(String t, String agentCode, String ip);
	
	/**
	 * 生成token
	 * @param userId 用户id
	 * @param agentCode 浏览器版本信息
	 * @param ip 用户真实ip地址
	 * @param sessionId 会话id
	 * @return token值
	 */
	public String createToken(Integer userId, String agentCode, String ip, String sessionId);
	
	/**
	 * 注销token
	 * @param token
	 */
	public void logoutToken(String token);
	
	/**
	 * 清空token表
	 */
	public void deleteTable();
}
