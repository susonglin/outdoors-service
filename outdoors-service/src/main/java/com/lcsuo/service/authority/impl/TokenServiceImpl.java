package com.lcsuo.service.authority.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.lcsuo.bean.authority.Token;
import com.lcsuo.dao.authority.TokenMapper;
import com.lcsuo.service.authority.TokenService;
import com.lcsuo.utils.DateUtils;
import com.lcsuo.utils.seculity.Md5PwdEncoder;

@Service
public class TokenServiceImpl implements TokenService {
	
	@Autowired 
	private TokenMapper dao;

	@Override
	public Token checkToken(String t, String agentCode, String ip) {
		if (StringUtils.isEmpty(t)) { // 如果token=空: 返回：null, token验证失败, exit;
			return null;
		}
		Token token = dao.checkToken(t,new Md5PwdEncoder().encodePassword(agentCode), ip);
		if (token == null) { // 如果没查到, 返回:null, token验证失败, exit;
			return null;
		}
		Date lastVisitTime = token.getLastupdatetime();
		Integer second = DateUtils.getSecondBetweenDate(lastVisitTime, new Date()); // 获取两个时间的差值秒
		// 检查是否过期: if (当前时间-上次访问时间) > 20分钟
		if (second / 60 > 20) {
			dao.deleteByPrimaryKey(token.getToken()); // 删除 token;
			return null; // 返回：null, token过期，exit;
		}
		// 更新最近访问时间: lastVisitTime=当前时间
		token.setLastupdatetime(new Date());
		dao.updateByPrimaryKey(token); // 更新token
		// 返回Token.
		return token;
	}

	@Override
	public String createToken(Integer userId, String agentCode, String ip, String sessionId) {
		// 1: 生成 token: token = MD5 (UserID+AgentCode+IP+SessionId);
		StringBuffer sf = new StringBuffer();
		sf.append(userId).append(agentCode).append(ip).append(sessionId);
		String md5token = new Md5PwdEncoder().encodePassword(sf.toString());
		// 2: 查找DB, 看有没有token:
		Token token = dao.selectByPrimaryKey(md5token);
		if (token != null) { // 如果有token存在, 更新 lastVisitTime=Now
			token.setLastupdatetime(new Date());
			token.setLastvisittime(new Date());
			token.setLastupdatorid(userId);
			dao.updateByPrimaryKeySelective(token);//更新token
		} else { // 不存在, 写入数据库: AgentCode = MD5(User-Agent)
			Token createToken = new Token();
			createToken.setToken(md5token);
			createToken.setUserid(userId);
			createToken.setAgentcode(new Md5PwdEncoder().encodePassword(agentCode)); // MD5(User-Agent)
			createToken.setIp(ip);
			createToken.setLastupdatetime(new Date());
			createToken.setCreatetime(new Date());
			dao.insert(createToken);
		}
		// 3) 返回token;
		return md5token;
	}

	@Override
	public void logoutToken(String token) {
		Token tokenBean = dao.selectByPrimaryKey(token);
		if(tokenBean != null){
			dao.deleteByPrimaryKey(token);
		}
	}

	@Override
	public void deleteTable() {
		dao.deleteTable();
	}
}
