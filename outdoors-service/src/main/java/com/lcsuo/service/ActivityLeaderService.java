package com.lcsuo.service;

import java.util.List;

import com.lcsuo.bean.AccountLeader;
import com.lcsuo.bean.ActivityLeader;
import com.lcsuo.bean.ThemeActivity;

public interface ActivityLeaderService {

	/**
	 * 
	 * @param activityLeader
	 * @param accountLeader
	 * @param themeActivity
	 */
	public void save(ActivityLeader activityLeader, AccountLeader accountLeader, ThemeActivity themeActivity);
	
	/**
     * 根据领队查询
     * @param accountLeader
     * @return
     */
    public List<ActivityLeader> findByAccountLeader(Integer accountLeader);
    
    /**
     * 根据活动查询
     * @param themeActivityId
     * @return
     */
    public List<ActivityLeader> findByThemeActivityId(Integer themeActivityId);
}
