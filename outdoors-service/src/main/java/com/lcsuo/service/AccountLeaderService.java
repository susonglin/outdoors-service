package com.lcsuo.service;

import java.util.List;

import com.lcsuo.bean.AccountLeader;
import com.lcsuo.bean.GroupTeam;

public interface AccountLeaderService {
	
	/**
	 * 新增领队
	 * @param accountLeader   领队信息
	 * @param groupTeam       团队信息
	 * @param accountId       领队Id
	 */
	public void saveAccountLeader(AccountLeader accountLeader, GroupTeam groupTeam, String accountId);
	
}
