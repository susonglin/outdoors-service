package com.lcsuo.service;

import com.lcsuo.bean.Account;
import com.lcsuo.bean.Theme;

public interface ThemeService {

	public void saveTheme(Theme theme, Integer groupId, Account user);
}
