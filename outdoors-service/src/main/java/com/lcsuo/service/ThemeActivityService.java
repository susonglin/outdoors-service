package com.lcsuo.service;

import java.util.List;

import com.lcsuo.bean.AccountLeader;
import com.lcsuo.bean.ThemeActivity;
import com.lcsuo.bean.vo.ActivityVo;

public interface ThemeActivityService {

	public void saveActivity(ActivityVo activityVo);
	
	public List<ThemeActivity> findByAccountId(Integer accountId);
}
