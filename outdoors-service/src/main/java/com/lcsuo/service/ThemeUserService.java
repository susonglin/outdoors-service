package com.lcsuo.service;

import com.lcsuo.bean.vo.ThemeUserVo;

public interface ThemeUserService {

	public void insertThemeUser(ThemeUserVo themeUserVo, Integer themeActivityId);
}
