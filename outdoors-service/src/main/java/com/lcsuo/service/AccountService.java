package com.lcsuo.service;

import com.lcsuo.bean.Account;

public interface AccountService {

	public Account get(Integer id);
	
	public void register(String mobileId, String password, String role);
	
	public Account login(String mobileId, String password);
}
