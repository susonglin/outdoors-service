package com.lcsuo.dao;

import com.lcsuo.bean.ThemeUser;

public interface ThemeUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ThemeUser record);

    int insertSelective(ThemeUser record);

    ThemeUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ThemeUser record);

    int updateByPrimaryKey(ThemeUser record);
}