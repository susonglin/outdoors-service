package com.lcsuo.dao.authority;

import org.apache.ibatis.annotations.Param;

import com.lcsuo.bean.authority.Token;

public interface TokenMapper {
    int deleteByPrimaryKey(String token);

    int insert(Token record);

    int insertSelective(Token record);

    Token selectByPrimaryKey(String token);

    int updateByPrimaryKeySelective(Token record);

    int updateByPrimaryKey(Token record);
    
	/**
	 * 清空token表
	 */
	public void deleteTable();
	
	/**
	 * 验证Token是否有效, 判断条件: token, 浏览器版本信息, ip
	 * @param t 用户携带的token值
	 * @param agentCode 浏览器版本信息
	 * @param ip ip地址
	 * @return 有效的token对象
	 */
	public Token checkToken(@Param("token") String t, @Param("agentCode") String agentCode, @Param("ip") String ip);
}