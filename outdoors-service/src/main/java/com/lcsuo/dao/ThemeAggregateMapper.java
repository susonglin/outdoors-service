package com.lcsuo.dao;

import com.lcsuo.bean.ThemeAggregate;

public interface ThemeAggregateMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ThemeAggregate record);

    int insertSelective(ThemeAggregate record);

    ThemeAggregate selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ThemeAggregate record);

    int updateByPrimaryKey(ThemeAggregate record);
}