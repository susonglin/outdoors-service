package com.lcsuo.dao;

import java.util.List;

import com.lcsuo.bean.ThemeActivity;

public interface ThemeActivityMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ThemeActivity record);

    int insertSelective(ThemeActivity record);

    ThemeActivity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ThemeActivity record);

    int updateByPrimaryKey(ThemeActivity record);
     
    public List<ThemeActivity> findByAccountId(Integer accountId);
}