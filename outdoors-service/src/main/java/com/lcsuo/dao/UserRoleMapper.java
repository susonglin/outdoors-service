package com.lcsuo.dao;

import com.lcsuo.bean.UserRole;

public interface UserRoleMapper {
    int insert(UserRole record);

    int insertSelective(UserRole record);
}