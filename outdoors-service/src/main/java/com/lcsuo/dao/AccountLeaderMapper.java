package com.lcsuo.dao;

import java.util.List;

import com.lcsuo.bean.AccountLeader;

public interface AccountLeaderMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AccountLeader record);

    int insertSelective(AccountLeader record);

    AccountLeader selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AccountLeader record);

    int updateByPrimaryKey(AccountLeader record);
    
    /**
     * 根据领导查询信息
     * @param accountId
     * @return
     */
    public List<AccountLeader> findByAccountId(Integer accountId);
}