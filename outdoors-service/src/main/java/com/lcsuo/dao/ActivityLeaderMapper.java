package com.lcsuo.dao;

import java.util.List;

import com.lcsuo.bean.ActivityLeader;

public interface ActivityLeaderMapper {
    int insert(ActivityLeader record);

    int insertSelective(ActivityLeader record);
    
    /**
     * 根据领队查询
     * @param accountLeader
     * @return
     */
    public List<ActivityLeader> findByAccountLeader(Integer accountLeader);
    
    /**
     * 根据活动查询
     * @param themeActivityId
     * @return
     */
    public List<ActivityLeader> findByThemeActivityId(Integer themeActivityId);
}