package com.lcsuo.dao;

import com.lcsuo.bean.ThemeVehicle;

public interface ThemeVehicleMapper {
    int insert(ThemeVehicle record);

    int insertSelective(ThemeVehicle record);
}