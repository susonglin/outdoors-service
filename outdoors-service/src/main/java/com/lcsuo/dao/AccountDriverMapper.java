package com.lcsuo.dao;

import com.lcsuo.bean.AccountDriver;

public interface AccountDriverMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AccountDriver record);

    int insertSelective(AccountDriver record);

    AccountDriver selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AccountDriver record);

    int updateByPrimaryKey(AccountDriver record);
}