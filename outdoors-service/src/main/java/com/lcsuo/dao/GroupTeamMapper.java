package com.lcsuo.dao;

import com.lcsuo.bean.GroupTeam;

public interface GroupTeamMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GroupTeam record);

    int insertSelective(GroupTeam record);

    GroupTeam selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GroupTeam record);

    int updateByPrimaryKey(GroupTeam record);
    
    public GroupTeam findByAccountId(Integer accountId);
}